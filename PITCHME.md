# Dependency Injection

tl;dr - <span style="color:orange">Dependency injection</span> is fucking awesome!

An implementation of <span style="color:orange">Inversion of Control (IoC)</span>.

---

## What's Dependency?
![Image-Absolute](assets/images/dependency.png)

---

## What's Dependency Injection?
![Image-Absolute](assets/images/dependency-injection.png)

---

## Let's See Some Codes
```java
/* Without DI */
class CoffeeMaker {
    private final Heater heater;
    private final Pump pump;

    CoffeeMaker() {
        this.heater = new ElectricHeater();
        this.pump = new Thermosiphon(heater);
    }

    Coffee makeCoffee() { /* */ }
}

class CoffeeMain {
    public static void main(String[] args) {
        Coffee coffee = 
        new CoffeeMaker().makeCoffee();
    }
}
```

@[16,17](A coffee maker is created to make coffee)
@[7,8](CoffeeMaker depends on ElectricHeater and Thermosiphon)

---

## Injecting Dependencies
```java
/* With Manual DI */
class CoffeeMaker {
    private final Heater heater;
    private final Pump pump;

    CoffeeMaker(Heater heater, Pump pump) {
        this.heater = checkNotNull(heater);
        this.pump = checkNotNull(pump);
    }

    Coffee makeCoffee() { /* */ }
}

class CoffeeMain {
    public static void main(String[] args) {
        Heater heater = new ElectricHeater();
        Pump pump = new Thermosiphon(heater);
        Coffee coffee = 
        new CoffeeMaker(heater, pump).makeCoffee();
    }
}
```

@[6,18,19](Here is the place we inject dependencies)

---

### Dependency Injection In Pictures

![Image-Absolute](assets/images/di.png)

---

## Brief History of DI

@fa[arrow-down]

+++

## 2002: <span style="color:orange">Spring 1 & 2</span> by SpringSource

+++

![Image-Absolute](assets/images/spring-di.png)

+++

![Image-Absolute](assets/images/spring-di-graph.png)

+++

mmm... We hate <span style="color:red">XML</span>!

+++

## 2006: <span style="color:orange">Guice</span> by Google

+++

![Image-Absolute](assets/images/guice-di-1.png)

+++

![Image-Absolute](assets/images/guice-di-2.png)

+++

![Image-Absolute](assets/images/guice-di-graph.png)

+++

Much better! At least we don't have XML anymore.
But it still has problem of <span style="color:red">runtime validation</span>.

+++

## 2013: <span style="color:orange">Dagger</span> by Square

+++

![Image-Absolute](assets/images/dagger1-di-graph.png)

+++

### Generated Codes
![Image-Absolute](assets/images/dagger1-di.png)

+++

## 2016: <span style="color:orange">Dagger 2</span> by Google

+++

### Generated Codes
![Image-Absolute](assets/images/dagger2-di-1.png)

+++

![Image-Absolute](assets/images/dagger2-di-2.png)

+++

![Image-Absolute](assets/images/dagger2-di-3.png)

+++

### Client API
![Image-Absolute](assets/images/dagger2-client-api-1.png)

+++

![Image-Absolute](assets/images/dagger2-client-api-2.png)

---

## Demo Time

+++

## Java Application
#### [Dagger 2](https://google.github.io/dagger)
#### [https://google.github.io/dagger](https://google.github.io/dagger/)

+++
## C Sharpe
#### [Ninject](http://www.ninject.org)
#### [http://www.ninject.org](http://www.ninject.org)

---

# Q&A
